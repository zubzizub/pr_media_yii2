<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Главная страница';
?>
<h1><?= $this->title ?></h1>
<p>Всего элементов: <?= count($posts) ?></p>

<p>
    <?php if (!Yii::$app->user->isGuest): ?>
        <?= Html::a('Create Post', ['/posts/create'], ['class' => 'btn btn-success']) ?>
    <?php endif; ?>
</p>
<div class="box">
    <div class="box-body">

        <?php foreach ($posts as $post): ?>
            <div class="post">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="<?= Url::to(['/posts/view', '_id' => (string)$post['_id']]) ?>">
                            <?= Html::encode($post['name']) ?>
                        </a>
                        <span> - <?= Yii::$app->formatter->asDateTime($post['created_at']); ?></span>
                        <span>  <?= $post['type'] ?></span>
                    </div>
                    <div class="panel-body">
                        <?= $post['text'] ?>
                        <?php
                            if (!Yii::$app->user->isGuest &&
                                Yii::$app->user->getId() == $post['userId']
                            ) {

                                echo Html::a(
                                    'delete',
                                    Url::to(['/posts/delete', '_id' => (string)$post['_id']]),
                                    [
                                        'class' => 'pull-right',
                                        'data-method' => 'post',
                                    ]
                                );
                            }
                        ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>