<?php

use yii\helpers\Html;

$this->title = 'Страница с отрывком';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="post_view">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Html::encode($post['name']) ?>
            <span> - <?= Yii::$app->formatter->asDateTime($post['created_at']); ?></span>
        </div>
        <div class="panel-body">
            <?= $post['text'] ?>
        </div>
    </div>
</div>

