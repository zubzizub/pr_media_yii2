<?php


/* @var $this yii\web\View */
/* @var $model app\forms\PostForm */

$this->title = 'Создание отрывка';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<div class="post_create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>