<?php

namespace app\forms;

use yii\base\Model;

class PostForm extends Model
{
    public $name;
    public $text;
    public $type;

    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['name', 'text'], 'required'],
            ['text', 'string', 'min' => 2, 'max' => 1000],
            ['type', 'integer']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name' => 'Название отрывка',
            'text' => 'Отрывок',
            'type' => 'Приватный отрывок'
        ];
    }
}