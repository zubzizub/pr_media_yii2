<?php

namespace app\entities;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\mongodb\ActiveRecord;

/**
 * @property string $name
 * @property string $text
 * @property integer $type
 * @property integer $userId
 */

class Post extends ActiveRecord
{
    const PUBLIC_POST = 0;

    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName(): string
    {
        return 'posts';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes(): array
    {
        return [
            '_id',
            'userId',
            'name',
            'type',
            'text',
            'created_at',
            'updated_at'
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function create($name, $text, $type): self
    {
        $posts = new static();
        $posts->name = $name;
        $posts->text = $text;
        $posts->type = (integer)$type;
        $posts->userId = Yii::$app->user->getId();

        return $posts;
    }

    public static function getPosts(): array
    {
        return Post::find()
            ->where(['or', ['userId' => Yii::$app->user->getId()], ['type' => self::PUBLIC_POST]])
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()
            ->limit(10)
            ->all();
    }

    /**
     * @param integer $_id
     * @return bool
     */
    public static function checkPermissionViewPost($_id): bool
    {
        return Post::find()
            ->where(['_id' => $_id, 'type' => self::PUBLIC_POST])
            ->exists();
    }
}