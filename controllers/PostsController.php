<?php

namespace app\controllers;

use app\entities\Post;
use app\forms\PostForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PostsController extends Controller
{
    const PUBLIC_POST = 0;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?'],
                        'matchCallback' => function () {
                            return Post::checkPermissionViewPost(Yii::$app->getRequest()->get('_id'));
                        }
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $form = new PostForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $post = Post::create($form->name, $form->text, $form->type);

            if ($post->save()) {
                return $this->redirect(['view', '_id' => (string)$post->_id]);
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param integer $_id
     * @return mixed
     */
    public function actionView($_id)
    {
        return $this->render('view', [
            'post' => $this->findModel($_id),
        ]);
    }

    /**
     * @param integer $_id
     * @return \yii\web\Response
     */
    public function actionDelete($_id)
    {
        $this->findModel($_id)->delete();

        return $this->redirect(['/site/index']);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}